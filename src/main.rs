// https://github.com/rust-lang/rust-analyzer/blob/master/lib/lsp-server/examples/goto_def.rs

use lsp_server::{Connection, ExtractError, Message, Request, RequestId, Response};
use lsp_types::{
    request::Completion, CompletionItem, CompletionItemKind, CompletionOptions, CompletionResponse,
    InitializeParams, InsertTextFormat, InsertTextMode, OneOf, ServerCapabilities,
};
use serde::{Deserialize, Serialize};
use std::error::Error;

fn main() -> Result<(), Box<dyn Error + Sync + Send>> {
    let args: Vec<String> = std::env::args().collect();
    let snippet_file = match args.get(1) {
        Some(val) => val.as_str(),
        None => "resources/snippets.json",
    };
    // Note that  we must have our logging only write out to stderr.
    eprintln!("starting generic LSP server");

    // Create the transport. Includes the stdio (stdin and stdout) versions but this could
    // also be implemented to use sockets or HTTP.
    let (connection, io_threads) = Connection::stdio();

    // Run the server and wait for the two threads to end (typically by trigger LSP Exit event).
    let server_capabilities = serde_json::to_value(&ServerCapabilities {
        definition_provider: Some(OneOf::Left(true)),
        completion_provider: Some(CompletionOptions {
            trigger_characters: Some(vec![String::from(".")]),
            ..Default::default()
        }),
        ..Default::default()
    })
    .unwrap();
    let initialization_params = connection.initialize(server_capabilities)?;
    main_loop(connection, initialization_params, snippet_file)?;
    io_threads.join()?;

    // Shut down gracefully.
    eprintln!("shutting down server");
    Ok(())
}

#[derive(Serialize, Deserialize, Debug)]
struct Snippet {
    prefix: String,
    body: Vec<String>,
    description: String,
}

fn main_loop(
    connection: Connection,
    params: serde_json::Value,
    snippet_file: &str,
) -> Result<(), Box<dyn Error + Sync + Send>> {
    let _params: InitializeParams = serde_json::from_value(params).unwrap();
    eprintln!("starting example main loop");
    for msg in &connection.receiver {
        eprintln!("got msg: {msg:?}");
        match msg {
            Message::Request(req) => {
                if connection.handle_shutdown(&req)? {
                    return Ok(());
                }
                match cast::<Completion>(req) {
                    Ok((id, params)) => {
                        eprintln!("got Completion request #{id}: {params:?}");
                        let snippets_str = std::fs::read_to_string(snippet_file).unwrap();
                        let snippets: Vec<Snippet> = serde_json::from_str(&snippets_str).unwrap();
                        let completion_items = snippets
                            .into_iter()
                            .map(|snippet| CompletionItem {
                                kind: Some(CompletionItemKind::SNIPPET),
                                label: snippet.prefix,
                                detail: Some(snippet.description),
                                insert_text: Some(
                                    snippet.body.iter().fold(String::new(), |acc, s| acc + s),
                                ),
                                insert_text_format: Some(InsertTextFormat::SNIPPET),
                                insert_text_mode: Some(InsertTextMode::ADJUST_INDENTATION),
                                ..Default::default()
                            })
                            .collect();
                        let completion_resp = Some(CompletionResponse::Array(completion_items));
                        let result = serde_json::to_value(&completion_resp).unwrap();
                        let resp = Response {
                            id,
                            result: Some(result),
                            error: None,
                        };
                        connection.sender.send(Message::Response(resp))?;
                        continue;
                    }
                    Err(err @ ExtractError::JsonError { .. }) => panic!("{err:?}"),
                    Err(ExtractError::MethodMismatch(req)) => req,
                };
                // ...
            }
            Message::Response(resp) => {
                eprintln!("got response: {resp:?}");
            }
            Message::Notification(not) => {
                eprintln!("got notification: {not:?}");
            }
        }
    }
    Ok(())
}

fn cast<R>(req: Request) -> Result<(RequestId, R::Params), ExtractError<Request>>
where
    R: lsp_types::request::Request,
    R::Params: serde::de::DeserializeOwned,
{
    req.extract(R::METHOD)
}
